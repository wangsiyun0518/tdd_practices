package tdd_practices.romantodecimal;

import java.util.HashMap;

public class RomanToDecimal {

    private HashMap<Character, Integer> dictionary;

    RomanToDecimal() {
        dictionary = new HashMap<>();
        dictionary.put('I', 1);
        dictionary.put('V', 5);
        dictionary.put('X', 10);
        dictionary.put('L', 50);
        dictionary.put('C', 100);
        dictionary.put('D', 500);
        dictionary.put('M', 1000);
    }

    public int romanToDecimal(String roman) {
        int index = roman.length() - 2;
        int res = this.dictionary.get(roman.charAt(index + 1));

        while (index >= 0) {
            if (this.dictionary.get(roman.charAt(index)) < this.dictionary.get(roman.charAt(index + 1))) {
                res -= this.dictionary.get(roman.charAt(index));
            } else {
                res += this.dictionary.get(roman.charAt(index));
            }
            index--;
        }
        return res;
    }
}
