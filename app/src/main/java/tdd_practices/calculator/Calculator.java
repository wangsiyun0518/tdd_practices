package tdd_practices.calculator;

import java.util.ArrayList;

public class Calculator {
    public static String negativeNums(String[] nums) {
        StringBuilder negativeNums = new StringBuilder();
        for (String num : nums) {
            if (!num.isEmpty()) {
                if (num.charAt(0) == '-') {
                    negativeNums.append(" ");
                    negativeNums.append(num);
                }
            }
        }
        return negativeNums.toString();
    }

    public int add(String numbers) throws NegativeNumberException{

        int sum = 0;
        if (numbers.isEmpty()) {
            return 0;
        } else if (!numbers.contains(",") && !numbers.contains("\n")) {

            return Integer.parseInt(numbers);
        } else if (!numbers.contains("//")){
            String newNumers = numbers.replace("\n", ",");

            for (int i = 1; i < newNumers.length(); i++) {
                if (newNumers.charAt(i) == ',' && newNumers.charAt(i-1) == ',') {
                    return -1;
                }
            }

            String[] nums = newNumers.split(",");

            for (String n : nums) {
                sum += Integer.parseInt(n);
            }
            return sum;
        } else {
            String remainingStr = numbers.substring(2);
            String delimiter = "\n";
            int indexNewLine = remainingStr.indexOf("\n");
            if (indexNewLine != 0) {
                delimiter = remainingStr.substring(0, indexNewLine);
            }
            String newNumers = remainingStr.replace("\n", delimiter);
            String[] nums = newNumers.split(delimiter);

            String negatives = negativeNums(nums);
            if (!negatives.isEmpty()) {
                throw new NegativeNumberException("negatives not allowed:" + negatives);
            } else {
                for (String n : nums) {
                    if (!n.isEmpty()) {
                        sum += Integer.parseInt(n);
                    }
                }
                return sum;
            }
        }
    }
}
