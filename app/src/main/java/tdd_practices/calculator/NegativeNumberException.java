package tdd_practices.calculator;

public class NegativeNumberException extends Exception{
    public NegativeNumberException(String message) {
        super(message);
    }
}
