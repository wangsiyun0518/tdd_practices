package tdd_practices.romantodecimal;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RomanToDecimalTest {
    private RomanToDecimal romanToDecimal;

    @BeforeEach
    void setup() {
        romanToDecimal = new RomanToDecimal();
    }

    @Test
    @DisplayName("Should Return Correct Decimal for Roman Input")
    void shouldReturnOneCorrectDecimal() {
        int expected = 1;
        int actual = romanToDecimal.romanToDecimal("I");
        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Should Return Correct Decimal for Complex Roman Input")
    void shouldReturnCorrectDecimal() {
        int expected = 1944;
        int actual = romanToDecimal.romanToDecimal("MCMXLIV");
        assertEquals(expected, actual);

        expected = 2006;
        actual = romanToDecimal.romanToDecimal("MMVI");
        assertEquals(expected, actual);
    }
}