package tdd_practices.calculator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {

    private Calculator calculator;

    @BeforeEach
    void setup() {
        calculator = new Calculator();
    }

    @Test
    @DisplayName("Calculator Should Return 0 For Empty String")
    void shouldReturnZero() throws NegativeNumberException {
        int expected = 0;
        int actual = calculator.add("");
        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Calculator Should Return Number For Single Number String")
    void shouldReturnNumber() throws NegativeNumberException {
        int expected = 1;
        int actual = calculator.add("1");
        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Calculator Should Return Sum For Two Numbers String")
    void shouldReturnSum() throws NegativeNumberException {
        int expected = 3;
        int actual = calculator.add("1,2");
        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Calculator Should Return Sum For All Numbers in String With New Line")
    void shouldReturnSumForAllNumbers() throws NegativeNumberException {
        int expected = 6;
        int actual = calculator.add("1\n2,3");
        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Calculator Should Return -1 For Invalid Input")
    void shouldReturnNegativeOneForInvalidInput() throws NegativeNumberException {
        int expected = -1;
        int actual = calculator.add("1\n,");
        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Calculator Should Return Sum For Custom Delimiter")
    void shouldReturnSumForCustomDelimiter() throws NegativeNumberException {
        int expected = 10;
        int actual = calculator.add("//;\n1;2;3;4");
        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Calculator Should throw exception when input has negative numbers")
    void shouldThrowExceptionForNegativeNumbers() {
        NegativeNumberException exception = assertThrows(NegativeNumberException.class, () -> {
            calculator.add("//;\n1;-2;3;-4");
        });
        String expected = "negatives not allowed: -2 -4";
        assertEquals(expected, exception.getMessage());
    }

    @Test
    @DisplayName("negativeNums should return empty string for positive numbers")
    void negativeNumsShouldReturnEmptyString() {
        String expected = "";
        String actual = Calculator.negativeNums(new String[]{"1", "3", "2"});
        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("negativeNums should return a string of negative numbers")
    void negativeNumsShouldReturnNegativeNumberString() {
        String expected = " -1 -2";
        String actual = Calculator.negativeNums(new String[]{"-1", "3", "-2"});
        assertEquals(expected, actual);
    }
}