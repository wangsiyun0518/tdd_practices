package tdd_practices.fizzbuzz;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FizzbuzzTest {
    private Fizzbuzz fizzbuzz;
    @BeforeEach
    void setup() {
        fizzbuzz = new Fizzbuzz();
    }

    @Test
    @DisplayName("when input is divisible by 3, return the string Fizz")
    void fizzbuzzShouldReturnFizz() {
        String expected = "Fizz";
        String actual = fizzbuzz.fizzbuzz(3);
        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("when input is divisible by 5, return the string Buzz")
    void fizzbuzzShouldReturnBuzz() {
        String expected = "Buzz";
        String actual = fizzbuzz.fizzbuzz(10);
        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("when input is divisible by both 3 and 5, return the string FizzBuzz")
    void fizzbuzzShouldReturnFizzBuzz() {
        String expected = "FizzBuzz";
        String actual = fizzbuzz.fizzbuzz(15);
        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("otherwise return number as a string")
    void fizzbuzzShouldReturnNumberString() {
        String expected = "17";
        String actual = fizzbuzz.fizzbuzz(17);
        assertEquals(expected, actual);
    }
}